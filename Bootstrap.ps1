<#
.SYNOPSIS
Get all powershell tooling.
#>
function Install-PsUtils {
  param (
    [parameter(Mandatory)] [String] $Directory
  )
  & git clone -q https://github.com/stinos/powershell.git $Directory
  if(-not ($LASTEXITCODE -eq 0)){
    throw "Inital clone failed"
  }
  Import-Module (Join-Path $Directory 'Modules\Ig')
}

<#
.SYNOPSIS
Get C++ dependencies via powershell.
.PARAMETER VcpkgTool
Path to vcpkg executable. If empty tries VcpkgTool from the environment variables,
or if that is also not found uses c:\tools\vcpkg\vcpkg.exe (which is the default on e.g. Appveyor).
.NOTES
Depends on Install-PsUtils.
#>
function Install-CppPackages {
  param (
    [parameter()] [String] $VcpkgTool
  )
  if(-not $VcpkgTool) {
    $VcpkgTool = $env:VcpkgTool
  }
  if(-not $VcpkgTool) {
    $VcpkgTool = 'c:\tools\vcpkg\vcpkg.exe'
  }
  Invoke-External { & $VcpkgTool install cppzmq:x86-windows }
  Invoke-External { & $VcpkgTool install cppzmq:x64-windows }
  Invoke-External { & $VcpkgTool install sdl2-mixer:x86-windows }
  Invoke-External { & $VcpkgTool install sdl2-mixer:x64-windows }
  Invoke-External { & $VcpkgTool integrate install }
}

<#
.SYNOPSIS
Get our .mrconfig file.
.DESCRIPTION
Clones neurofys mrconfig repository and copies the config to mrConfigFile.
#>
function Get-MrConfig {
  param (
    [parameter()] [String] $MrConfigCloneDir = (Join-Path $(Get-Location) 'mrconfig'),
    [parameter()] [String] $MrConfigFile = (Join-Path $(Get-Location) '.mrconfig'),
    [parameter()] [AllowEmptyCollection()] [Hashtable[]] $Hosts = @(),
    [parameter()] [Boolean] $Quiet = $True
  )

  Update-GitRepo $MrConfigCloneDir (Convert-SshAddress git@bitbucket.org:neurofys/mrconfig $Hosts) -Shallow -Quiet
  Copy (Join-Path $MrConfigCloneDir '.mrconfig') $MrConfigFile
}

<#
.SYNOPSIS
Clone all repositories from mrconfig
.DESCRIPTION
If the directory doesn't exist yet the repository is cloned, else it is updated.
#>
function Get-Repositories {
  param (
    [parameter()] [ValidateScript({Test-Path $_})] [String] $MrConfigFile = (Join-Path $(Get-Location) '.mrconfig'),
    [parameter(Mandatory)] [ValidateSet('newest', 'current', 'mr')] [String] $Type,
    [parameter()] [String[]] $MrInsteadOfNewest = @(),
    [parameter()] [AllowEmptyCollection()] [Hashtable[]] $Hosts = @(),
    [parameter()] [Boolean] $Shallow = $True,
    [parameter()] [Boolean] $Quiet = $True
  )

  Get-MrRepos $mrConfigFile | % {
    Write-Host '[mr] ' $_.directory
    $remote = Convert-SshAddress $_.remote $Hosts
    if($Type -eq 'newest') {
      if($MrInsteadOfNewest.Contains($_.name)) {
        Update-GitRepo $_.directory $remote -Branch $_.branch -Shallow:$Shallow -Quiet:$Quiet
      }
      else {
        Update-GitRepo $_.directory $remote -UseNewest -Shallow:$Shallow -Quiet:$Quiet
      }
    }
    elseif($Type -eq 'current') {
      Update-GitRepo $_.directory $remote -Shallow:$Shallow -Quiet:$Quiet
    }
    elseif($Type -eq 'mr') {
      Update-GitRepo $_.directory $remote -Branch $_.branch -Shallow:$Shallow -Quiet:$Quiet
    }
    else {
      throw 'Unrecognized option'
    }
    # Show the version we are using.
    git -C $_.directory log -n1 --oneline
  }
}

<#
.SYNOPSIS
Get last (by committer date) commit from mr repositories.
#>
function Get-LastCommit {
  param (
    [parameter()] [ValidateScript({Test-Path $_})] [String] $MrConfigFile = (Join-Path $(Get-Location) '.mrconfig')
  )

  Get-MrRepos $MrConfigFile | %{
    @{'repo'=$_ ; 'commit'=(Invoke-Git -Directory $_.directory log -n1 --all --date=raw --format='''%cd %h %cn %ce''')}
  } | Sort-Object {$_.commit.Split(' ')[0]} | Select-Object -Last 1 | %{
    $cm = $_.commit.Split(' ')
    $_['hash'] = $cm[2]
    $_['comitter'] = $cm[3]
    $_['email'] = $cm[4]
    $_
  }
}

<#
.SYNOPSIS
Download binary ffmpeg distribution and put it in the expected location.
#>
function Get-FFMpegBinaries {
  param (
    [parameter()] [ValidateScript({Test-Path $_})] [String] $ProjectDir = (Get-Location),
    [parameter()] [String] $version = '^ffmpeg-n.*-win64-lgpl-shared-.*'
  )

  function SubDir {
    (Join-Path $ProjectDir $args[0])
  }

  $assets = (Invoke-RestMethod -Method Get https://api.github.com/repos/BtbN/FFmpeg-Builds/releases/latest).assets
  $url = ($assets | ?{$_.name -cmatch $version})[0].browser_download_url
  Ig\Expand-FromWeb $url (SubDir 'ffmpeg_x64') -ErrorAction Stop

  '_include', '_lib\ffmpeg_x64', '_bin\ffmpeg_x64' | %{mkdir (SubDir $_) -ErrorAction SilentlyContinue}

  Copy -Recurse -Force (SubDir "ffmpeg_x64\*\include\*") (SubDir '_include\')
  Copy -Recurse -Force (SubDir "ffmpeg_x64\*\lib\*.lib") (SubDir '_lib\ffmpeg_x64\')
  Copy -Recurse -Force (SubDir "ffmpeg_x64\*\bin\*.dll") (SubDir '_bin\ffmpeg_x64\')

  Remove-Item -Recurse -Force (SubDir 'ffmpeg_x64')
}

<#
.SYNOPSIS
Provide configurable release builds.
#>
function Invoke-TnsBuild {
  [CmdletBinding(SupportsShouldProcess)]
  param (
    [parameter()] [String] $ProjectDir = (Get-Location),
    [parameter()] [String] $DistDir = (Join-Path (Get-Location) '_dist'),
    [parameter()] [String] $BuildProps = '',
    [Switch] $X64,
    [Switch] $X86,
    [Switch] $NoNTX,
    [Switch] $NoVISA,
    [Switch] $NoLabview,
    [Switch] $NoPrep,
    [Switch] $NoPreReq,
    [Switch] $NoBuild,
    [ValidateSet('none', 'binary', 'source')] [String] $FFMpeg = 'none',
    [Switch] $DebugConfig
  )

  if(-not $X64 -and -not $X86) {
    throw 'Exactly one of -X86 / -X64 must be passed'
  }

  $BuildOpts = "/p:DistDir=$DistDir\"
  if($BuildProps) {
    $BuildOpts += ";${BuildProps}"
  }
  $PreReqOpts = '/p:'
  $PrepOpts = '/p:'
  if($NoNTX) {
    $BuildOpts += ';NoNTX=True'
    $PreReqOpts += 'BuildINtime=False'
  } else {
    $PreReqOpts += 'BuildINtime=True'
  }
  if($NoLabview) {
    $PreReqOpts += ';BuildLabview=False'
  } else {
    $PreReqOpts += ';BuildLabview=True'
  }
  if($NoVISA) {
    $BuildOpts += ';NoVISA=True'
  }
  if($DebugConfig) {
    $BuildOpts += ';Configuration=Debug;DistPrefix=_distdebug'
  } else {
    $BuildOpts += ';Configuration=Release'
  }
  switch($FFMpeg) {
    'none' { $PrepOpts += 'NoFFMPEG=True' }
    'source' { $PrepOpts += 'NoFFMPEG=False' }
    'binary' {
      $PrepOpts += 'NoFFMPEG=True'
      if(-not $NoPrep -and $PSCmdlet.ShouldProcess('Get FFMpeg', "Get-FFMpegBinaries $ProjectDir")) {
        Get-FFMpegBinaries $ProjectDir -ErrorAction Stop
      }
    }
  }
  $BuildOpts += ';Platform='
  $BuildOpts += if($X86) { "Win32" } else { "x64" }

  if(-not $NoPrep -and $PSCmdlet.ShouldProcess('Prepare', ("Building in {0}, PrepOpts {1}" -f $ProjectDir, $PrepOpts))) {
    Exec { msbuild (Join-Path $ProjectDir 'build/prepareworkspace.targets') $PrepOpts } -ErrorAction Stop
  }

  if(-not $NoPreReq -and $PSCmdlet.ShouldProcess('Build PreReq', ("Building in {0}, PreReqOpts {1}" -f $ProjectDir, $PreReqOpts))) {
    Exec { msbuild (Join-Path $ProjectDir 'build/createrelease.targets') "/t:PreReq" $PreReqOpts } -ErrorAction Stop
  }

  if(-not $NoBuild -and $PSCmdlet.ShouldProcess('Build', ("Building in {0}, BuildOpts {1}" -f $ProjectDir, $BuildOpts))) {
    Exec { msbuild (Join-Path $ProjectDir 'build/createrelease.targets') "/t:CreateVariant" $BuildOpts } -ErrorAction Stop
  }
}

<#
.SYNOPSIS
Default bootstrap/build for a CI server.

Note that because of this issue https://github.com/appveyor/ci/issues/1090 we don't use 
'-ErrorAction Stop' for the tests, but mimick it with ErrorVariable followed by throw.
Also since we sometimes submit pull requests for micropython always use the branch specified in
mrconfig instead of a possibly newer branch based on master which will fail the build anyway.
#>
function Invoke-TnsBootstrap {
  param (
    [parameter()] [String] $ProjectDir = (Get-Location),
    [parameter()] [Hashtable[]] $Hosts = @(),
    [parameter()] [Hashtable] $Keys = @{},
    [parameter(Mandatory)] [ValidateSet('newest', 'current', 'mr')] [String] $Type
  )

  mkdir -Force $ProjectDir
  Write-Host -ForegroundColor Green 'Installing global prerequisites'
  Install-PsUtils (Join-Path $ProjectDir 'powershell')
  Install-SshConfig $Hosts $Keys
  Install-CppPackages

  Write-Host -ForegroundColor Green 'Cloning repositories'
  Get-MrConfig (Join-Path $ProjectDir 'mrconfig') (Join-Path $ProjectDir '.mrconfig') $Hosts
  Get-Repositories (Join-Path $ProjectDir '.mrconfig') $Type $Hosts -MrInsteadOfNewest @('micropython')
  . (Join-Path $ProjectDir 'tnstests\systemtest\RunTests.ps1')

  Write-Host -ForegroundColor Green 'Installing/building local prerequisites'
  $distDir = (Join-Path $ProjectDir '_dist')
  Invoke-TnsBuild $ProjectDir $distDir -NoNTX -NoVISA -NoLabview -FFMpeg 'binary' -NoBuild -X86

  Write-Host -ForegroundColor Green 'Building x86'
  Remove-Item $distDir -Recurse -Force -ErrorAction SilentlyContinue
  Invoke-TnsBuild $ProjectDir $distDir -NoNTX -NoVISA -NoLabview -FFMpeg 'binary' -NoPrep -NoPreReq -X86
  Invoke-TnsSystemTests (Get-Item "$distDir\*\_distx86") -ErrorVariable testsError
  if($testsError) {
    throw $testsError
  }
  Invoke-TnsUnitTests (Get-Item "$distDir\*\_distx86") -ErrorVariable testsError
  if($testsError) {
    throw $testsError
  }

  Write-Host -ForegroundColor Green 'Building x64'
  Remove-Item $distDir -Recurse -Force -ErrorAction SilentlyContinue
  Invoke-TnsBuild $ProjectDir $distDir -NoNTX -NoVISA -NoLabview -FFMpeg 'binary' -NoPrep -NoPreReq -X64
  Invoke-TnsSystemTests (Get-Item "$distDir\*\_distx64") -ErrorVariable testsError
  if($testsError) {
    throw $testsError
  }
  Invoke-TnsUnitTests (Get-Item "$distDir\*\_distx64") -ErrorVariable testsError
  if($testsError) {
    throw $testsError
  }

  Invoke-TnsCunitTests (Get-Item "$distDir\*\_distx64") -ErrorVariable testsError
  if($testsError) {
    throw $testsError
  }
}


